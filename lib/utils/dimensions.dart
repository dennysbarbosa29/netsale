import 'package:flutter/material.dart';

///Gives us methods to get the screen size and dimensions constants
///
///[Dimensions.height(context)] and [Dimensions.width(context)]
///
///[Dimensions.padding]
///
class Dimensions{
  static double height(BuildContext context) => MediaQuery.of(context).size.height;

  static double width(BuildContext context) => MediaQuery.of(context).size.width;

  // Padding definitions
  static const double defaultPadding = 20.0;
  static const double topPadding = 20.0;
  static const double sidePadding = 16.0;
  static const double inBetweenItensPadding = 5.0;
   static const double inBetweenItensService = 15.0;

  ///Returns a [SizedBox] with only `height` defined by `inBetweenItensPadding`
  static Widget heightSpacer = SizedBox(height: Dimensions.inBetweenItensPadding,);
  ///Returns a [SizedBox] with only `width` defined by `inBetweenItensPadding`
  static Widget widthSpacer = SizedBox(width: Dimensions.inBetweenItensPadding,);

  static Widget widthSpacerService = SizedBox(width: Dimensions.inBetweenItensService,);

  //Border width Def

}
