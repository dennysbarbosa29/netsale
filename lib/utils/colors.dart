import 'package:flutter/material.dart';

class StandardColors {
  static const Color primaryColor = Colors.white;

  static  Color secondaryColor =Colors.grey[300];

  static const Color tertiaryColor = Colors.white;

  static const Color quaternaryColor = Color(0xff084444);

  static const Color contourColor = Colors.grey;

  static const Color textButton = Colors.white;

  static const Color textName = Color(0xff727A8C);

  static const Color textPhone = Color(0xffB1B7C5);
  
  static const Color iconOnSale = Colors.red;

  static  Color iconOfSale = Colors.grey[400];

  static  Color iconOfService = Colors.grey[600];

  static  Color iconOnService = Colors.green;

}
