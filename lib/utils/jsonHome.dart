import 'dart:ffi';
import 'dart:convert';

Map<String, dynamic>  netsaleServer  =  json.decode("""{
  "explorer":{
    "list_clients": [
      {
        "name": "José Augusto",
        "location": {
          "latitude": "-22.865074",
          "longitude": "-43.220892"
        },
        "phone": "23156398",
        "salespotential": false,
        "cep": "23659520",
        "address": "Rua das Pedrinhas",
        "contract": {
          "id": "0315236-1",
          "list_services": [
            {
              "title": "Internet",
              "subtitle": "Ativo",
              "status": true,
              "image": "wifi"
            },
            {
              "title": "TV",
              "subtitle": "Ativo",
              "status": true,
              "image": "tv"
            },
            {
              "title": "phone",
              "subtitle": "Ativo",
              "status": true,
              "image": "phone"
            }
          ]
        }
      },
      {
        "name": "Douglas Siqueira",
        "location": {
          "latitude": "-22.929835",
          "longitude": "-43.174503"
        },
        "phone": "23156398",
        "salespotential": true,
        "cep": "23659521",
        "address": "Rua das Couves",
        "contract": {
          "id": "0315236-3",
          "list_services": [
            {
              "title": "Internet",
              "subtitle": "Serviço não habilitado",
              "status": true,
              "image": "wifi"
            },
            {
              "title": "TV",
              "subtitle": "Ativo",
              "status": true,
              "image": "tv"
            },
            {
              "title": "phone",
              "subtitle": "Ativo",
              "status": false,
              "image": "phone"
            }
          ]
        }
      },
      {
        "name": "Dennys Barbosa",
        "location": {
          "latitude": "-23.604015",
          "longitude": "-46.662877"
        },
        "phone": "23156398",
        "salespotential": true,
        "cep": "23659520",
        "address": "Rua das Pedrinhas",
        "contract": {
          "id": "0315236-1",
          "list_services": [
            {
              "title": "Internet",
              "subtitle": "Serviço não habilitado",
              "status": false,
              "image": "wifi"
            },
            {
              "title": "TV",
              "subtitle": "Serviço não habilitado",
              "status": false,
              "image": "tv"
            },
            {
              "title": "phone",
              "subtitle": "Serviço não habilitado",
              "status": false,
              "image": "phone"
            }
          ]
        }
      },
      {
        "name": "Lucas Cetem",
        "location": {
          "latitude": "-21.890",
          "longitude": "-43.390955"
        },
        "phone": "23156398",
        "salespotential": true,
        "cep": "23659520",
        "address": "Rua das Pedrinhas",
        "contract": {
          "id": "0315236-1",
          "list_services": [
            {
              "title": "Internet",
              "subtitle": "Ativo",
              "status": true,
              "image": "wifi"
            },
            {
              "title": "TV",
              "subtitle": "Ativo",
              "status": true,
              "image": "tv"
            },
            {
              "title": "phone",
              "subtitle": "Serviço não habilitado",
              "status": false,
              "image": "phone"
            }
          ]
        }
      }
    ]
  }


}""");

/*  Colocar no Começo se o douglas decidir assim
    // "Pessoa": {
  //   "Nome": "Douglas",
  //   "Local": {
  //     "latitude": 22.030,
  //     "longitude": 22.030,
  //   },
  // },

*/