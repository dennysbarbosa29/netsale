import 'package:location/location.dart';

Future<bool> checkLocationPermition(Location locationService)async{
 bool _permission = false;
 try {
      bool serviceStatus = await locationService.serviceEnabled(); // Service Verify
      if (serviceStatus) {
        _permission = await locationService.requestPermission();
        if (_permission) 
          return true;
        return false;
      }
      //If permission is pending, request Allow permission
      else {
        bool pendingResult = await locationService.requestService(); // Request Permition;
        return pendingResult;
      }
     } catch (error) { 
     throw error;
    }
 }
