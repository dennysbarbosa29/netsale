import 'dart:convert';

class RadiusJson {
  Map<String, dynamic> radiusJson3Position() {
    return json.decode("""{
    "radius":[
      500, 
      2000, 
      3000
    ]
  }""");
  }

  Map<String, dynamic> radiusJson2Position() {
    return json.decode("""{
  "radius":[
    500, 2000
  ]
  }""");
  }

  Map<String, dynamic> radiusJsonNoKey() {
    return json.decode("""{
  "":[
    500, 2000, 3000
  ]
}""");
  }

  Map<String, dynamic> radiusJsonStrangeKey() {
    return json.decode("""{
  "r":[
    500, 2000, 3000
  ]
}""");
  }
}
