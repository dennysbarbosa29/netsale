import 'package:flutter/material.dart';
import 'package:net_sale/ui_layer/views/Screen/client_details.dart';
import 'package:net_sale/ui_layer/views/Screen/home_screen.dart';
import 'package:net_sale/ui_layer/views/Screen/login_screen.dart';
import 'package:net_sale/ui_layer/views/Screen/radius_slider.dart';
import 'package:net_sale/ui_layer/views/splash_screen_view.dart';




const String homeRoute = '/';
const String loginRoute = 'login';
const String splashRoute = 'splash'; 
const String formScreen = 'formScreen';
const String clientDetails = 'clientDetails';
const String radiusSlider = 'radiusSlider';


class Router {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case homeRoute:
        return MaterialPageRoute(builder: (_) => HomeScreenView());
       case loginRoute:
         return MaterialPageRoute(builder: (_) => LoginScreen());
      case splashRoute:
        return MaterialPageRoute(builder: (_) => SplashScreenView());
      case radiusSlider:
        return MaterialPageRoute(builder: (_) => RadiusSlider());
      // case formScreen:
      //   return MaterialPageRoute(builder: (_)=> DiscardFormScreen());
      case clientDetails:
        //return MaterialPageRoute(builder: (_)=> ClientDetails());
      
      default:
        return MaterialPageRoute(
          builder: (_) {
            return Scaffold(
              body: Center(
                child: Text('BUG: Rota não definida para ${settings.name}'),
              ),
            );
          },
        );
    }
  }
}
