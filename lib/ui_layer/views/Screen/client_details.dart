import 'package:flutter/material.dart';
import 'package:net_sale/NetSale.dart';
import 'package:net_sale/main.dart';
import 'package:net_sale/models/client_model.dart';
import 'package:net_sale/ui_layer/widgets/explore_bottom_bar.dart';
import 'package:net_sale/ui_layer/widgets/row_details.dart';
import 'package:net_sale/ui_layer/widgets/row_disponse.dart';
import 'package:net_sale/ui_layer/widgets/row_services.dart';
import 'package:net_sale/ui_layer/widgets/service_info.dart';
import 'package:net_sale/utils/colors.dart';
import 'package:net_sale/utils/dimensions.dart';
import 'package:provider/provider.dart';

class ClientDetails extends StatefulWidget {
  Client clientIndex;
  ClientDetails(this.clientIndex);
  static BuildContext context;

  @override
  State<StatefulWidget> createState() {
    return ClientDetailsState(clientIndex);
  }
}

class ClientDetailsState extends State<ClientDetails> {
  Client clientIndex;
  ClientDetailsState(this.clientIndex);
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    NetSale.firstpage += 1;
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    NetSale.firstpage -= 1;
  }

  @override
  Widget build(BuildContext context) {
    ClientDetails.context = context;
    return Scaffold(
        appBar: AppBar(
            backgroundColor: StandardColors.tertiaryColor,
            automaticallyImplyLeading: false,
            title:
                RowDetailsBuilder(clientIndex.name, clientIndex.phone).build()),
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                padding: EdgeInsets.all(20),
                color: StandardColors.secondaryColor,
                child: Column(
                  children: <Widget>[
                    for (int i = 0;
                        i < clientIndex.contract.typeservices.length;
                        i++)
                      ServiceInfo(
                        status: clientIndex.contract.typeservices[i].status,
                        title: clientIndex.contract.typeservices[i].title,
                        subtitle: clientIndex.contract.typeservices[i].subtitle,
                        icon: clientIndex.contract.typeservices[i].image,
                      ),
                  ],
                ),
              ),

              Container(
                padding: EdgeInsets.all(20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      clientIndex.address,
                      //"Rua Barra do Bacururu, 127 - Jd. \nVivam, São Paulo,SP - CEP: 02912-010",
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                        color: StandardColors.textName,
                      ),
                    ),
                    SizedBox(
                      height: Dimensions.height(context) * 0.02,
                    ),
                    Text(
                      "Contrato: ${clientIndex.contract.id}",
                      style: TextStyle(
                        fontSize: 13,
                        fontWeight: FontWeight.bold,
                        color: StandardColors.textName,
                      ),
                    ),
                    SizedBox(
                      height: Dimensions.height(context) * 0.02,
                    ),
                    RowDisponse(),
                    SizedBox(
                      height: Dimensions.height(context) * 0.02,
                    ),
                    RowServices(
                        tv: !clientIndex.contract.typeservices[0].status,
                        wifi: !clientIndex.contract.typeservices[1].status,
                        phone: !clientIndex.contract.typeservices[2].status),
                  ],
                ),
              ),
//
            ],
          ),
        ),
        bottomNavigationBar: ExploreBottomBar());
  }
}
