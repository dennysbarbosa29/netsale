import 'dart:async';

import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:net_sale/control_layer/explore_provaider.dart';
import 'package:net_sale/models/radius_model.dart';
import 'package:net_sale/ui_layer/router.dart';
import 'package:net_sale/ui_layer/views/fragments/map_fragment.dart';
import 'package:net_sale/ui_layer/widgets/explore_bottom_bar.dart';
import 'package:net_sale/utils/colors.dart';
import 'package:net_sale/utils/dimensions.dart';
import 'package:provider/provider.dart';

class HomeScreenView extends StatefulWidget {
  // Completer<GoogleMapController> _controller = Completer();
  //static const LatLng _center = const LatLng(45.521563, -122.677433);

  @override
  State<StatefulWidget> createState() {
    return _HomeScreenViewState();
  }
}

class _HomeScreenViewState extends State<HomeScreenView> {
  GlobalKey _selected = new GlobalKey();
  void _onMapCreated(GoogleMapController controller) {
    //_controller.complete(controller);
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        //drawer: drawer(),
        body: Container(
            height: Dimensions.height(context),
            width: Dimensions.width(context),
            child: Stack(children: <Widget>[
              MapPage(),
              Positioned(
                top: 10,
                right: 10,
                left: 10,
                child: Container(
                  color: Colors.transparent,
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Consumer<ExploreProvider>(
                            builder: (context, exploreProvider, _) {
                          return PopupMenuButton(
                            color: Colors.white,
                            icon: Icon(Icons.more_vert),
                            itemBuilder: (context) {
                              var list = List<PopupMenuEntry<Object>>();
                              for (int i = 0;
                                  i < exploreProvider.radiusList.length;
                                  i++) {
                                list.add(
                                  PopupMenuItem(
                                      //  key: Key(exploreProvider.radiusList[i].toString()),
                                      value: exploreProvider.radiusList[i]
                                          .toString(),
                                      child: Text(exploreProvider.radiusList[i]
                                              .toString() +
                                          ' M')),
                                );
                                if (i < (exploreProvider.radiusList.length - 1))
                                  list.add(PopupMenuDivider(height: 10));
                              }

                              return list;
                            },
                            initialValue: 1,
                            // onSelected: (value){_selected = value;}, //TODO Yuri  aqui está bugado
                          );
                        })
                      ]),
                ),
              ),
            ])),
        bottomNavigationBar: ExploreBottomBar(),

        //     Positioned(
        //         top: 10,
        //         right: 15,
        //         left: 15,
        //         child: Container(
        //           color: Colors.transparent,
        //           child: Row(
        //             children: <Widget>[
        //               IconButton(
        //                 splashColor: Colors.grey,
        //                 icon: Icon(Icons.menu),
        //                 onPressed: (){

        //                 },
        //               ),
        //               IconButton()

        //             ],
        //           ),
        //         ),

        // ),
      ),
    );
  }
}
// Widget drawer() => Drawer(
//             child: ListView(
//               padding: EdgeInsets.zero,
//               children: <Widget>[
//                 _createDrawerItem(icon: Icons.adjust,text: 'Raio',onTap:() => Navigator.pushReplacementNamed(context, radiusSlider)),
//                   ],
//                 ),
//               );

Widget _createDrawerItem(
    {IconData icon, String text, GestureTapCallback onTap}) {
  return ListTile(
    title: Row(
      children: <Widget>[
        Icon(icon),
        Padding(
          padding: EdgeInsets.only(left: 8.0),
          child: Text(text),
        )
      ],
    ),
    onTap: onTap,
  );
}

// appBar: AppBar(
//   backgroundColor: Colors.transparent,
//   title: Text("Netsale"),
//   actions: <Widget>[
//     PopupMenuButton(
//       icon: Icon(Icons.more_horiz),
//       itemBuilder: (context) => [
//         PopupMenuItem(
//           child:Text('1km'),
//         ),
//         PopupMenuItem(child: Text("2km"),),
//       ]
//         )
//       ],
//     ),
