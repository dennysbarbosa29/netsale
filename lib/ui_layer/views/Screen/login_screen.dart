import 'package:flutter/material.dart';
import 'package:net_sale/control_layer/login_controller.dart';
import 'package:action_widget/ActionListenerWidget.dart';
import 'package:action_widget/EventWidgetAnim.dart';

import '../../router.dart';
import 'home_screen.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<LoginScreen> with ActionListenerWidget {
  TextStyle style = TextStyle(fontSize: 20.0);
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  FocusNode _loginTextFieldFocusNode = FocusNode();
  FocusNode _passwordTextFieldFocusNode = FocusNode();

  String _login;
  String _password;
  LoginController loginController = LoginController();
  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;

    // TODO: get strings according to phone language
    String invalidLoginErrorMessage = 'Login inválido';
    String invalidPasswordErrorMessage = 'Senha inválida';
    String loginText = 'Login';

    String initialLogin = 'demo';
    String initialPassword = 'demo';

    TextFormField emailField = TextFormField(
      obscureText: false,
      style: style,
      decoration: InputDecoration(
          fillColor: Colors.grey,
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          hintText: "Email",
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(8.0)),
          focusedBorder: OutlineInputBorder(
            borderSide: const BorderSide(color: Colors.grey, width: 2.0),
            borderRadius: BorderRadius.circular(8.0),
          )),
      textInputAction: TextInputAction.next,
      focusNode: _loginTextFieldFocusNode,
      validator: (value) {
        print('login validator');
        if (value.isEmpty) {
          return invalidLoginErrorMessage;
        }
        _login = value;
        return null;
      },
      onFieldSubmitted: (_) =>
          FocusScope.of(context).requestFocus(_passwordTextFieldFocusNode),
      initialValue: initialLogin,
    );

    TextFormField passwordField = TextFormField(
      obscureText: true,
      style: style,
      decoration: InputDecoration(
          //focusColor: Colors.black,
          //hoverColor: Colors.black,
          fillColor: Colors.black,
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          hintText: "Password",
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(8.0)),
          focusedBorder: OutlineInputBorder(
            borderSide: const BorderSide(color: Colors.grey, width: 2.0),
            borderRadius: BorderRadius.circular(8.0),
          )),
      textInputAction: TextInputAction.done,
      focusNode: _passwordTextFieldFocusNode,
      validator: (value) {
        if (value.isEmpty) {
          return invalidPasswordErrorMessage;
        }
        _password = value;
        print('Valor da senha: $value');
        return null;
      },
      onFieldSubmitted: (_) async {
        if (_formKey.currentState.validate()) {
          bool loggedIn = await loginController.submitLogin(
              _login, _password, _scaffoldKey);
          if (loggedIn) {
            Navigator.pushReplacementNamed(context, homeRoute);
          }
        }
      },
      initialValue: initialPassword,
    );

    EventWidgetAnim loginButton = EventWidgetAnim(
      actionListener: this,
      child: Material(
        key: Key("id_btn_login"),
        elevation: 5.0,
        borderRadius: BorderRadius.circular(8.0),
        color: Colors.blue,
        child: MaterialButton(
          padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),

          child: Text("Sign In",
              textAlign: TextAlign.center,
              style: style.copyWith(
                  color: Colors.white, fontWeight: FontWeight.bold)),
        ),
      ),
    );

    return Scaffold(
      key: _scaffoldKey,
      body: Container(
        color: Colors.white,
        height: screenHeight,
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizedBox(height: screenHeight * 0.05),
              SizedBox(
                height: screenHeight * 0.25,
                child: Image.asset(
                  "assets/images/logo_open_labs.png",
                  fit: BoxFit.contain,
                ),
              ),
              SizedBox(
                height: screenHeight * 0.01,
              ),
              Card(
                  elevation: 5,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10))),
                  child: Column(children: <Widget>[
                    Container(
                      height: screenHeight * 0.06,
                      width: screenWidth,
                      decoration: BoxDecoration(
                        color: Colors.grey[200],
                        shape: BoxShape.rectangle,
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(8),
                            topRight: Radius.circular(8)),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Align(
                          alignment: Alignment.centerLeft,
                          child: FittedBox(
                            fit: BoxFit.fitHeight,
                            child: Text(
                              loginText,
                              style: style.copyWith(
                                  color: Colors.black54,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                      ),
                      //color: Colors.grey,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 8, right: 8),
                      child: SizedBox(height: screenHeight * 0.01),
                    ),
                    Form(
                      key: _formKey,
                      child: Column(children: [
                        Padding(
                          padding: const EdgeInsets.only(left: 8, right: 8),
                          child: emailField,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 8, right: 8),
                          child: SizedBox(height: screenHeight * 0.01),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 8, right: 8),
                          child: passwordField,
                        ),
                      ]),
                    ),
                    Container(
                      height: screenHeight * 0.05,
                      width: screenWidth / 0.12,
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.only(right: 8),
                              child: FlatButton(
                                  textColor: Colors.blue,
                                  onPressed: () {},
                                  child: Text("Forgot Password?",
                                      textAlign: TextAlign.end)),
                            ),
                          ]),
                    ),
                  ])),
              SizedBox(height: screenHeight * 0.02),
              Container(
                height: screenHeight * 0.09,
                width: screenWidth / 0.15,
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(right: 8),
                        child: loginButton,
                      ),
                    ]),
              ),
              SizedBox(height: screenHeight * 0.02),
              Divider(color: Colors.black),
              SizedBox(height: screenHeight * 0.02),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  FloatingActionButton(
                    heroTag: "googleButton",
                    backgroundColor: Colors.white,
                    child: Image.asset('assets/images/googlelogo.png'),
                    onPressed: () {},
                  ),
                  SizedBox(width: screenWidth / 15),
                  FloatingActionButton(
                    heroTag: "facebookButton",
                    backgroundColor: Colors.white,
                    child: Image.asset('assets/images/facebooklogo.png'),
                    onPressed: () {},
                  ),
                ],
              ),
              Container(
                height: screenHeight * 0.06,
                width: screenWidth / 1.30,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      'New User?',
                      style: TextStyle(color: Colors.black),
                    ),
                    SizedBox(width: screenWidth / 60),
                    FlatButton(
                      textColor: Colors.blue,
                      onPressed: () {},
                      child: Text("Sign Up"),
                    ),
                  ],
                ),
              )
            ],
          ),
          // ),
        ),
      ),
    );
  }

  @override
  void manageTapClick(String value) {
    switch (value) {
      case 'id_btn_login':
        goHome();
        break;
    }

    // TODO: implement manageTapClick
  }

  void goHome() async {
    if (_formKey.currentState.validate()) {
      bool loggedIn =
          await loginController.submitLogin(_login, _password, _scaffoldKey);
      if (loggedIn) {
        Navigator.pushReplacementNamed(context, homeRoute);
      }
    }
  }
}
