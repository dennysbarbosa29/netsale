// import 'dart:async';
// import 'dart:typed_data';
// import 'package:flutter/material.dart';
// import 'package:google_maps_flutter/google_maps_flutter.dart';
// import 'package:location/location.dart';
// import 'package:flutter/services.dart';
// import 'package:net_sale/control_layer/explore_provaider.dart';
// import 'package:net_sale/utils/app_permissions.dart';

// import 'dart:ui' as ui;

// import 'package:provider/provider.dart';


// import 'package:google_maps_flutter/google_maps_flutter.dart';

// const LatLng _center = const LatLng(33.738045, 73.084488);
// final Set<Marker> _markers = {};
// final Set<Polyline>_polyline={};

// //add your lat and lng where you wants to draw polyline
// LatLng _lastMapPosition = _center;
// List<LatLng> latlng = [LatLng(33.738045, 73.084488), LatLng(33.567997728, 72.635997456)];
// LatLng _new = LatLng(33.738045, 73.084488);
// LatLng _news = LatLng(33.567997728, 72.635997456);

// // latlng.add(_new);
// // latlng.add(_news);

// //call this method on button click that will draw a polyline and markers

// void _onAddMarkerButtonPressed() {
//     getDistanceTime();
//     setState(() {
//         _markers.add(Marker(
//             // This marker id can be anything that uniquely identifies each marker.
//             markerId: MarkerId(_lastMapPosition.toString()),
//             //_lastMapPosition is any coordinate which should be your default 
//             //position when map opens up
//             position: _lastMapPosition,
//             infoWindow: InfoWindow(
//                 title: 'Really cool place',
//                 snippet: '5 Star Rating',
//             ),
//             icon: BitmapDescriptor.defaultMarker,

//         ));
//         _polyline.add(Polyline(
//             polylineId: PolylineId(_lastMapPosition.toString()),
//             visible: true,
//             //latlng is List<LatLng>
//             points: latlng,
//             color: Colors.blue,
//         ));
//     });

//     //in your build widget method
//     GoogleMap(
//     //that needs a list<Polyline>
//         polylines:_polyline,
//         markers: _markers,
//         onMapCreated: _onMapCreated,
//         myLocationEnabled:true,
//         onCameraMove: _onCameraMove,
//         initialCameraPosition: CameraPosition(
//             target: _center,
//             zoom: 11.0,
//         ),

//         mapType: MapType.normal,

//     );
// }