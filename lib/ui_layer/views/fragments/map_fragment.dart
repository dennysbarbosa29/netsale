import 'dart:async';
import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:flutter/services.dart';
import 'package:net_sale/control_layer/explore_provaider.dart';
import 'package:net_sale/utils/app_permissions.dart';

import 'dart:ui' as ui;

import 'package:provider/provider.dart';

class MapPage extends StatefulWidget {
  const MapPage({Key key}) : super(key: key);
  @override
  _MapPageState createState() => _MapPageState();
}

class _MapPageState extends State<MapPage> with AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => true;
  GoogleMapController mapController;
  LatLng _lastMapPosition = LatLng(-22.864494, -43.220302);
  List<LatLng> points = [LatLng(-22.864356, -43.220055), LatLng(-22.866134, -43.222992),LatLng(-22.864272, -43.219918),LatLng(-22.864277, -43.219666),LatLng(-22.864255, -43.219621),LatLng(-22.864412, -43.219530),LatLng(-22.864465, -43.219465),
  LatLng(-22.864476, -43.219388),LatLng(-22.864455, -43.219302),LatLng(-22.864348, -43.219176),LatLng(-22.864338, -43.219144),LatLng(-22.864276, -43.218355)];
  List<LatLng> spPoints = [LatLng(-23.600046, -46.659007),LatLng(-23.612655, -46.670844)];
  List<LatLng> flaPoints = [LatLng(-22.929766, -43.177529),LatLng(-22.930186, -43.174250)];

  void _onMapCreated(GoogleMapController controller) {
    _setStyle(controller);
    mapController = controller;
  }
  void _currentLocation() async {
    LocationData
          location = await _locationService.getLocation();
        CameraUpdate cameraUpdate = CameraUpdate.newCameraPosition(
            CameraPosition(bearing:0, target: LatLng(location.latitude, location.longitude),zoom: 17));
        mapController.animateCamera(cameraUpdate);
        _addPolilyne();
  }

  void _addPolilyne(){
    setState((){
      _polylines.add(Polyline(
      polylineId: PolylineId("Flamengo"),
      visible: true,
      points: flaPoints,
      color: Colors.green,
      ));
    _polylines.add(Polyline(
      polylineId: PolylineId("SP"),
      visible: true,
      points: spPoints,
      color: Colors.green,
      ));
    _polylines.add(Polyline(
      polylineId: PolylineId("teste incubadra"),
      visible: true,
      points: points,
      color: Colors.green,

    ));
  });
  }
  //Map<MarkerId, Marker> markers = <MarkerId, Marker>{};
  List<Marker> markers = <Marker>[];
  final Set<Polyline> _polylines = {};

  Completer<GoogleMapController> _mapController = Completer();
  static BuildContext contextTest;

  Future<Uint8List> getBytesFromAsset(String path, int width) async {
    ByteData data = await rootBundle.load(path);
    ui.Codec codec = await ui.instantiateImageCodec(data.buffer.asUint8List(),
        targetWidth: width);
    ui.FrameInfo fi = await codec.getNextFrame();
    return (await fi.image.toByteData(format: ui.ImageByteFormat.png))
        .buffer
        .asUint8List();
  }

  List<LatLng> polilynePoints = [];

  @override
  void initState() {
    super.initState();
    initPlatformState();
    //  viewListMaker();
  }

  double _startZoom = 17;
  LatLng _startPoint = LatLng(15, 515);

  Location _locationService = Location();
  String error;

  initPlatformState() async {
    await _locationService.changeSettings(
        accuracy: LocationAccuracy.HIGH, interval: 10);
    LocationData location;

    try {
      bool _permission = await checkLocationPermition(_locationService);
      if (_permission) {
        location = await _locationService.getLocation();
        CameraUpdate cameraUpdate = CameraUpdate.newLatLng(
            LatLng(location.latitude, location.longitude));
        mapController.moveCamera(cameraUpdate);
       // print("test se passa aqui count 0 vezes");
      } else {
        //TODO: Show Permission Denied page
      }
    } on PlatformException catch (e) {
      print(e);
      if (e.code == 'PERMISSION_DENIED') {
        error = e.message;
      } else if (e.code == 'SERVICE_STATUS_ERROR') {
        error = e.message;
      }
      location = null;
    }
  }

  @override
  Widget build(BuildContext context) {
    contextTest = context;
    return SafeArea(
      child: Consumer<ExploreProvider>(
        builder: (context, exploreProvider, _) {
          markers = exploreProvider.getMarkers();

          return Stack(
            children: <Widget>[
                GoogleMap(
                myLocationButtonEnabled: false,
                minMaxZoomPreference: MinMaxZoomPreference(10, 25),
                myLocationEnabled: true,
                zoomGesturesEnabled: true,
                polylines: _polylines,
                //zoomGesturesEnabled: false ,
                mapType: MapType.normal,
                
                initialCameraPosition:
                    CameraPosition(target: _startPoint, zoom: _startZoom),
                onMapCreated: _onMapCreated,
                markers: Set<Marker>.of(markers),
                //markers: Set<Marker>.of(markers.values),
                //markers: Set.from(mapsMarker),
                compassEnabled: true,
            ),
            Positioned(
              bottom: 65,
              right: 20,
              child: FloatingActionButton(
                backgroundColor: Colors.white,
                onPressed: _currentLocation,
                child: Icon(Icons.my_location, size: 30,),
              )),
            ]
          );
        },
      ),
    );
  }
    void _setStyle(GoogleMapController controller) async {
    String value = await DefaultAssetBundle.of(context).loadString('assets/json/maps_style.json');
    controller.setMapStyle(value);
  }
}
