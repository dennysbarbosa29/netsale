
import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';
import 'package:net_sale/ui_layer/views/Screen/login_screen.dart';

import '../router.dart';

class SplashScreenView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    return Scaffold(
          body: Container(
            color: Colors.white,
            alignment: Alignment.center,
            child: FlareActor(
              "assets/flare/logoDaOpen.flr",
              alignment: Alignment.center,
              fit: BoxFit.contain,
              animation: "go",
              callback: (_) async {
                Navigator.pushReplacementNamed(context, loginRoute);
                
                
              },
            ),
          ),
        );
  }
}
