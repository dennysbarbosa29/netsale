import 'package:flutter/material.dart';
import 'package:net_sale/utils/dimensions.dart';

import 'explore_drawer.dart';

class ExploreBottomBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BottomAppBar(
        child: GestureDetector(
      onTap: () {
        print("teste");
        showModalBottomSheet<Null>(
            context: context,
            builder: (BuildContext context) => _getExploreDrawer());
      },
      child: Container(
        height: Dimensions.height(context) * 0.08,
        child: Column(
          children: <Widget>[
            Icon(Icons.location_on),
            Text(
              'Explore',
              style: TextStyle(color: Colors.black),
            )
          ],
        ),
      ),
    ));
  }
}

Widget _getExploreDrawer() {
  return ExploreDrawer();
}
