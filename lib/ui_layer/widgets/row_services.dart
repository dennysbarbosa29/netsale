import 'package:flutter/material.dart';
import 'package:net_sale/utils/colors.dart';

class RowServices extends StatelessWidget {
  bool tv;
  bool wifi;
  bool phone;

  RowServices({
    Key key,
    @required this.tv,
    @required this.wifi,
    this.phone, // TODO: change this to required then we implement a provider
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Icon(
          Icons.wifi,
          color: wifi
              ? StandardColors.iconOnService
              : StandardColors.iconOfService,
          size: 50,
        ),
        Icon(
          Icons.tv,
          color:
              tv ? StandardColors.iconOnService : StandardColors.iconOfService,
          size: 50,
        ),
        Icon(
          Icons.phone,
          color: phone
              ? StandardColors.iconOnService
              : StandardColors.iconOfService,
          size: 50,
        ),
      ],
    );
  }
}
