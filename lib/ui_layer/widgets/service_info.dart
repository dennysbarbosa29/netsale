import 'package:flutter/material.dart';
import 'package:net_sale/utils/colors.dart';
import 'package:net_sale/utils/dimensions.dart';

class ServiceInfo extends StatelessWidget {
  // IconData icon;
  // bool ativo;
  String title;
  String subtitle;
  bool status;
  String icon;

  ServiceInfo({
    Key key,
    @required this.icon,
    @required this.subtitle,
    @required this.title,
    @required this.status,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          // mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Icon(
              icon == "wifi"
                  ? Icons.wifi
                  : icon == "phone"
                      ? Icons.phone
                      : icon == "tv" ? Icons.tv : Icons.image,
              color: status ? Colors.green : Colors.grey[600],
              size: 50,
            ),
            Dimensions.widthSpacerService,
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  title,
                  style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                    color: StandardColors.textName,
                  ),
                ),
                Text(
                  status ? "Ativo" : "Serviço não habilitado",
                  style: TextStyle(
                    fontSize: 16,
                    color: StandardColors.textPhone,
                  ),
                )
              ],
            ),
          ],
        ),
        SizedBox(
          height: Dimensions.height(context) * 0.05,
        ),
      ],
    );
  }
}
