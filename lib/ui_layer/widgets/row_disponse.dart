import 'package:flutter/material.dart';
import 'package:net_sale/utils/colors.dart';

class RowDisponse extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Text(
          "Serviço Disponiveis a Região",
          style: TextStyle(
            fontSize: 13,
            fontWeight: FontWeight.bold,
            color: StandardColors.textName,
          ),
        ),
        Icon(Icons.keyboard_arrow_up, color: StandardColors.iconOfSale,)
      ],
    );
  }
}
