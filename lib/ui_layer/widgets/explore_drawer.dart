import 'package:flutter/material.dart';
import 'package:net_sale/control_layer/explore_provaider.dart';
import 'package:net_sale/ui_layer/views/Screen/client_details.dart';
import 'package:net_sale/ui_layer/widgets/row_details.dart';
import 'package:net_sale/utils/colors.dart';
import 'package:net_sale/utils/dimensions.dart';
import 'package:provider/provider.dart';

import '../../NetSale.dart';
import '../router.dart';

class ExploreDrawer extends StatelessWidget {
  String title;
  Widget child;
  Color borderColor;

  // ExploreDrawerState({
  //   Key key,
  //   @required this.title,
  //   @required this.child,
  //   this.borderColor,
  // }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ThemeData theme = Theme.of(context);

    return Drawer(
      child: Container(
        padding: const EdgeInsets.all(8.0),
        child: Consumer<ExploreProvider>(
          builder: (BuildContext context, exploreProvider, _) {
            if (exploreProvider.exploreList.length == 0) {
              return Container(
                  child: Center(
                      child: CircularProgressIndicator(
                          backgroundColor: Colors.white)));
            } else {
              return Stack(
                children: <Widget>[
                  Padding(
                    child: ListView.builder(
                      itemCount: exploreProvider.exploreList.length,
                      itemBuilder: (BuildContext context, int index) {
                        return GestureDetector(
                          behavior: HitTestBehavior.translucent, // List Explore
                          child: Column(
                            children: <Widget>[
                              RowDetailsBuilder(
                                      exploreProvider.exploreList[index].name,
                                      exploreProvider.exploreList[index].phone)
                                  .setIcons(Icon(
                                    Icons.info,
                                    color: (exploreProvider
                                            .exploreList[index].salesPotential)
                                        ? (StandardColors.iconOnSale)
                                        : (StandardColors.iconOfSale),
                                  ))
                                  .build(),
                              SizedBox(
                                height: Dimensions.height(context) * 0.01,
                              ),
                              //Dimensions.heightSpacer
                            ],
                          ),
                          onTap: () {
                            if (NetSale.firstpage < 1) {
                              Route route = MaterialPageRoute(
                                  builder: (context) => ClientDetails(
                                      exploreProvider.exploreList[index]));
                              Navigator.pushReplacement(context, route);
                            } else {
                              Navigator.pop(ClientDetails.context);
                              Route route = MaterialPageRoute(
                                  builder: (context) => ClientDetails(
                                      exploreProvider.exploreList[index]));
                              Navigator.pushReplacement(context, route);
                            }
                          },
                        );
                      },
                    ),
                    padding: EdgeInsets.only(bottom: 10),
                  ),
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: Container(
                      width: 150,
                      height: 30,
                      child: ListTile(
                        title: Text(
                          'Version: 0.0.1',
                          style:
                              TextStyle(color: StandardColors.secondaryColor),
                        ),
                        onTap: () {},
                      ),
                    ),
                  )
                ],
              );
            }
          },
        ),
      ),
    );
  }
}
