import 'package:flutter/material.dart';
import 'package:net_sale/utils/colors.dart';
import 'package:net_sale/utils/dimensions.dart';

class RowDetails {
  String _name;
  String _phone;
  Widget _icon;

  RowDetails(RowDetailsBuilder rowDetailsBuilder) {
    this._name = rowDetailsBuilder.name;
    this._phone = rowDetailsBuilder.phone;
    this._icon = rowDetailsBuilder.icon;
  }

  String getName(String name) {
    return this._name;
  }

  String getPhone(String phone) {
    return this._phone;
  }

  Icon getIcon(Icon icon) {
    return this._icon;
  }

  Widget row() {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            CircleAvatar(
              backgroundColor: StandardColors.contourColor,
            ),
            Dimensions.widthSpacer,
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  _name,
                  style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                    color: StandardColors.textName,
                  ),
                ),
                Text(
                  _phone,
                  style: TextStyle(
                    fontSize: 16,
                    color: StandardColors.textPhone,
                  ),
                ),
              ],
            ),
          ],
        ),
        this._icon
      ],
    );
  }
}

class RowDetailsBuilder {
  String name;
  String phone;
  Widget icon = Container();

  RowDetailsBuilder(String name, String phone) {
    this.name = name;
    this.phone = phone;
  }

  //option
  RowDetailsBuilder setIcons(Widget icon) {
    this.icon = icon;
    return this;
  }

  Widget build() {
    return RowDetails(this).row();
  }
}
