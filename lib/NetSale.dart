import 'package:flutter/material.dart';
import 'package:net_sale/ui_layer/router.dart';
import 'package:net_sale/utils/colors.dart';
import 'package:provider/provider.dart';

import 'control_layer/explore_provaider.dart';



class NetSale extends StatelessWidget {

  static AppBarTheme _appBarTheme() => AppBarTheme(
      color: Colors.transparent, elevation: 0.0);
  
  static int firstpage = 0;
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        //start provider's

        ChangeNotifierProvider<ExploreProvider>(
          builder: (_) => ExploreProvider(),
        )
      ],
    child: MaterialApp(
      title: 'NetSale',
      debugShowCheckedModeBanner: false,
      

      theme: ThemeData(
        appBarTheme: _appBarTheme(),
        //scaffoldBackgroundColor: Colors.transparent,
        primaryColor:
            StandardColors.primaryColor, // StandardColors.primaryColor,
        backgroundColor: StandardColors.secondaryColor,
        // Theme and text styles definitions
        primarySwatch: StandardColors
            .contourColor, //Colors.grey, //Colors.deepPurple, //Colors.blueGrey,
        //TODO : define subhead -> dropdownbutton itens use this
        //primaryTextTheme: TextTheme(title: TextStyle(color: StandardColors.primaryColor)),
        textTheme: TextTheme(
          title: TextStyle(
            fontFamily: 'Corben',
            fontWeight: FontWeight.w600,
            fontSize: 20,
            //color: Colors.orange,
            color: StandardColors.primaryColor,
            // color: Colors.grey[900],
          ),
          subtitle: TextStyle(
            fontFamily: 'Corben',
            fontWeight: FontWeight.w400,
            fontSize: 18,
            color: StandardColors.secondaryColor,
            // color: Colors.grey[700],
          ),
          body1: TextStyle(
            fontFamily: 'Corben',
            fontWeight: FontWeight.w400,
            fontSize: 18,
            color: StandardColors.secondaryColor,
            //color: Colors.grey[350],
          ),
          display1: TextStyle(
            fontFamily: 'Corben',
            fontWeight: FontWeight.w400,
            fontSize: 18,
            color: StandardColors.secondaryColor,
            //color: Colors.grey[350],
          ),
        ),
      ),
      onGenerateRoute: Router.generateRoute,
      initialRoute: splashRoute, //splashRoute,
    ));
  }
}