class Localization {
  String latitude;
  String longitude;

  Localization.fromJson(Map<String, dynamic> parsedJson) {
    latitude = parsedJson['latitude'];
    longitude = parsedJson['longitude'];
  }

  // Localization(String latitude, String longitude){
  //   this.latitude = latitude;
  //   this.longitude = longitude;
  // }

}
