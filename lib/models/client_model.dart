import 'package:net_sale/models/contract_model.dart';
import 'package:net_sale/models/localization_model.dart';
import 'package:net_sale/models/person_model.dart';

class Client extends Person {
  String phone;
  bool salesPotential;
  String cep;
  String address;
  Contract contract;

  
  Client.fromJson(Map<String, dynamic> parsedJson) : super(parsedJson["name"],   Localization.fromJson(parsedJson['location']) ) {
    phone = parsedJson["phone"];
    salesPotential = parsedJson["salespotential"];
    cep = parsedJson["cep"];
    address =parsedJson["address"];
    contract = Contract.fromJson(parsedJson["contract"]);
  }

  // Client(String name, String phone, bool salesPotential, String cep,
  //     String endereco, Contract contract, Localization localization) : super(name, localization) {
  //   this.phone = phone;
  //   this.salesPotential = salesPotential;
  //   this.cep = cep;
  //   this.endereco = endereco;
  //   this.contract = contract;
  // }





  // Client.fromJson(Map<String, dynamic> parseJson) {

  // }

}
