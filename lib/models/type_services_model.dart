class TypeService {
  String title;
  String subtitle;
  bool status;
  String image;

  TypeService.fromJson(Map<String, dynamic> parsedJson) {
    
    title = parsedJson['title'];
    subtitle = parsedJson['subtitle'];
    status = parsedJson['status']; 
    image = parsedJson['image'];
  }

  // TypeService(String title, bool status, String image){
  //   this.title = title;
  //   this.status = status;
  //   this.image = image;
  // }

  String getSubtitle() {
    return status ? "blabla" : "bleblei";
  }
}
