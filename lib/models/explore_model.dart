import 'package:net_sale/models/client_model.dart';

class Explore {
  List<Client>  clients = [];

  Explore.fromJson(Map<String, dynamic> parsedJson) {

    for (int i = 0; i < (parsedJson['list_clients'].length); i++) {
      clients.add(Client.fromJson(parsedJson['list_clients'][i]));
    }
  }


}