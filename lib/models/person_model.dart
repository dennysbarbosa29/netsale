import 'localization_model.dart';

class Person {
  String name;
  Localization localization;

  Person.fromJson(Map<String, dynamic> parsedJson){
    name = parsedJson["name"];
    localization = Localization.fromJson(parsedJson);

  }

  Person(String name, Localization localization){
    this.name = name;
    this.localization = localization;
  }

}