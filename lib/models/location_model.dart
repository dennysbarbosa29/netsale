class Location {
  String latitude;
  String longitude;

  Location(String latitude, String longitude){
    this.latitude = latitude;
    this.longitude = longitude;
  }

}