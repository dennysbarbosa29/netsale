class RadiusOptions {
  List<int> radiusList;

  RadiusOptions.fromJson(Map<String, dynamic> parsedJson) {
    try {
      radiusList = [];
      for (int i = 0; i < (parsedJson['radius'].length); i++) {
        radiusList.add(parsedJson['radius'][i]);
      }
    } catch (error) {
      radiusList = null;
      print(error.toString());
    }
  }
}
