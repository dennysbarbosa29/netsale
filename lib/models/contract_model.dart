import 'package:net_sale/models/type_services_model.dart';

class Contract {
  String id;
  List<TypeService> typeservices = [];

  Contract.fromJson(Map<String, dynamic> parsedJson) {
    id = parsedJson['id'];
    var list = parsedJson['list_services'];
    if(list != null){
    for (int i = 0; i < list.length; i++) {

      TypeService typeService = TypeService.fromJson(parsedJson['list_services'][i]);
      typeservices.add(typeService);
     }
    }
  }

  // Contract(String id, List<TypeService> typeservices) {
  //   this.id = id;
  //   this.typeservices = typeservices;
  // }
}
