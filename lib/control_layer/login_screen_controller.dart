// import 'package:flutter/material.dart';
// import 'package:flutter/services.dart';
// import 'package:indicador_descarte_inadequado/ui/controllers/base_controller.dart';

// enum FormType { Login, SingUp }

// class LoginScreenController extends BaseController {
//   FormType _formType;
//   LoginScreenController() {
//     _formType = FormType.Login;
//   }
//   final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

//   // Private fields vars
//   String _password;
//   String _errorCode;

//   set email(value) => User.email = value;
//   set password(value) => _password = value;
//   set name(value) => User.displayName = value;
//   set phoneNumber(value) => User.phoneNumber = value;
//   set formType(FormType value) {
//     _formType = value;
//     notifyListeners();
//   }

//   GlobalKey<FormState> get formKey => _formKey;
//   FormType get formType => _formType;
//   String get errorCode => _errorCode;

//   bool _validateAndSaveFields() {
//     final _formState = _formKey.currentState;
//     if (_formState.validate()) {
//       _formState.save();
//       return true;
//     } else {
//       return false;
//     }
//   }

//   Future<bool> handleSingInSingUp() async {
//     //TODO : put authentication functions in a separate file
//     if (_validateAndSaveFields()) {
//       // set view as busy, so it shows a loading indicator
//       this.setState(ViewState.Busy);
//       if (_formType == FormType.Login)
//         try {
//           User.uid = (await FirebaseAuth.instance.signInWithEmailAndPassword(
//               email: User.email, password: _password)).user.uid;
//           this.setState(ViewState.Idle);
//           return true;
//         } on PlatformException catch (error) {
//           // TODO : Map errors to a corresponding message to return
//           this.setState(ViewState.Idle);
//           this._errorCode = error.code;
//           return false;
//         }
//       else if (_formType == FormType.SingUp)
//         try {
//           User.uid= (await FirebaseAuth.instance.createUserWithEmailAndPassword(
//             email: User.email,
//             password: _password,
//           )).user.uid;
//           this.setState(ViewState.Idle);
//           return true;
//         } on PlatformException catch (error) {
//           // TODO : Map errors to a corresponding message to return
//           this.setState(ViewState.Idle);
//           this._errorCode = error.code;
//           return false;
//         }
//     }
//     return null;
//   }
// }
