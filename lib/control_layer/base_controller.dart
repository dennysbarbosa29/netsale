// base_model.dart
import 'package:flutter/material.dart';

/// Represents the state of the view
enum ViewState { Idle, Busy }

class BaseController extends ChangeNotifier {
  ViewState _state = ViewState.Idle;
  ViewState get state => _state;
  void setState(ViewState viewState) {
    _state = viewState;
    notifyListeners();
  }
}
