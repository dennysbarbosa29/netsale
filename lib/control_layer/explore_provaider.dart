import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:net_sale/models/client_model.dart';
import 'package:net_sale/repository_layer/home_repository.dart';
import 'package:net_sale/utils/json_radius.dart';

class ExploreProvider with ChangeNotifier {
  List<Client> _exploreList = [];
  List<Marker> clientMarkers = [];
  List<int> radiusList;

  final HomeRepository _homeRepository = HomeRepository();

  ExploreProvider() {
    _exploreList = _homeRepository.fetchClient();
    radiusList =
        _homeRepository.fetchRadius((RadiusJson().radiusJson3Position()));
  }

  get exploreList => _exploreList;


  List<Marker> getMarkers() {
    for (int i = 0; i < _exploreList.length; i++) {
      String latString = _exploreList[i].localization.latitude;
      var lat = double.parse(latString);
      String longString = _exploreList[i].localization.longitude;
      var long = double.parse(longString);
      clientMarkers.add(
        Marker(
            markerId: MarkerId(_exploreList[i].name),
            icon:
                BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueRed),
            position: LatLng(lat, long),
            infoWindow: InfoWindow(
                title: _exploreList[i].name, snippet: _exploreList[i].phone)),
      );
    }
    return clientMarkers;
  }
}
