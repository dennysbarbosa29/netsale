import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:net_sale/NetSale.dart';
import 'package:net_sale/ui_layer/router.dart';
import 'package:net_sale/utils/colors.dart';
import 'package:provider/provider.dart';
import 'control_layer/explore_provaider.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);

  runApp(NetSale());
}


