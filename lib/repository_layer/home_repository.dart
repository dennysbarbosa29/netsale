import 'package:net_sale/models/client_model.dart';
import 'package:net_sale/models/explore_model.dart';
import 'package:net_sale/models/radius_model.dart';
import 'package:net_sale/utils/jsonHome.dart';

class HomeRepository {
  List<Client> fetchClient() {
    //Location currentLocation
    Map<String, dynamic> parsedJson;
    parsedJson = netsaleServer;
    return Explore.fromJson(parsedJson['explorer']).clients;
  }


 List<int> fetchRadius(Map<String, dynamic> parsedJson) {
    RadiusOptions teste = RadiusOptions.fromJson(parsedJson);
    return  teste.radiusList;
  }
}
