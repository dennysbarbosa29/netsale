import 'package:flutter_test/flutter_test.dart' as prefix0;
import 'package:net_sale/repository_layer/home_repository.dart';
import 'package:net_sale/utils/json_radius.dart';


main() {
  HomeRepository listRadius;

  prefix0.setUp(() {
    listRadius = HomeRepository();
  });
  prefix0.test("List clients in radius radiusJson2Position", () {
     List<int> radiusOptions = listRadius.fetchRadius(RadiusJson().radiusJson2Position());
       
    prefix0.expect(radiusOptions, [500, 2000]);
  });


  prefix0.test("List clients in radius radiusJson3Position ", () {
     List<int> radiusOptions = listRadius.fetchRadius(RadiusJson().radiusJson3Position());
       
    prefix0.expect(radiusOptions, [500, 2000, 3000]);
    
  });

  
  prefix0.test("List clients in radius radiusJsonNoKey ", () {
     List<int> radiusOptions = listRadius.fetchRadius(RadiusJson().radiusJsonNoKey());
       
    prefix0.expect(radiusOptions, null);
  });

  prefix0.test("List clients in radius radiusJsonStrangeKey ", () {
     List<int> radiusOptions = listRadius.fetchRadius(RadiusJson().radiusJsonStrangeKey());
       
    prefix0.expect(radiusOptions, null);
  });

  
}

